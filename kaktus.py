#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Author: Jan Svoboda <janis.svobi@gmail.com>

import os
import sys
import requests
from bs4 import BeautifulSoup as bs
from typing import Optional


ERR_FETCH_PAGE = 1
ERR_NEWS_NOT_FOUND = 2
ERR_SEND_MSG = 3
ERR_TELEGRAM_CONF = 4

url = 'https://www.mujkaktus.cz/homepage'
news_file = 'novinky.txt'


def read_telegram_config_from_env() -> Optional[dict[str,str]]:
    try:
        token = os.environ['telegram_bot_token']
        chat_id = os.environ['telegram_chat_id']
    except KeyError:
        return None

    return {'token': token, 'chat_id': chat_id}


def read_telegram_config_from_file() -> dict[str,str]:
    config = {}

    lines = None
    try:
        with open('telegram.conf', 'r') as f:
            lines = f.readlines()
    except FileNotFoundError:
        print('Chyba: Soubor telegram.conf nenalezen!', file=sys.stderr)
        sys.exit(ERR_TELEGRAM_CONF)

    # telegram.conf must contain 2 lines: telegram bot token and chat id
    error = False
    if len(lines) != 2:
        print('Chyba: Konfigurační soubor telegram.conf má neplatný obsah!', file=sys.stderr)
        error = True

    for line in lines:
        tokens = line.split(' = ')
        if tokens[0] == 'token':
            config['token'] = tokens[1].strip()
        elif tokens[0] == 'chat_id':
            config['chat_id'] = tokens[1].strip()
        else:
            print(f'Chyba: Konfigurační soubor telegram.conf obsahuje neplatný klíč: {tokens[0]}', file=sys.stderr)
            error = True

    if error:
        sys.exit(ERR_TELEGRAM_CONF)

    return config


def fetch_page() -> requests.models.Response:
    page = requests.get(url)
    if page.status_code != requests.codes.ok:
        print(f'Nezdařilo se stáhnout stránku \'{url}\'\nHTTP kód: {page.status_code}', file=sys.stderr)
        sys.exit(ERR_FETCH_PAGE)

    return page


def scrape_news_from_the_pot():
    page = fetch_page()
    soup = bs(page.content, 'html.parser')

    results = soup.find('div', class_='article ico Default')
    if not results:
        # The web page has changed, the HTML element we're looking for is not there.
        print('Novinky z květináče se nepodařilo najít, asi se změnila struktura stránky.', file=sys.stderr)
        sys.exit(ERR_NEWS_NOT_FOUND)

    # Title of the news
    elements = results.find_all('h3', class_='uppercase text-drawn text-green')
    news_name = elements[0].text
    content = news_name
    content += '\n'

    # Content of the news
    elements = results.find_all('p')
    news_content = elements[0].text
    content += news_content

    return content


def fresh_news(news: str) -> bool:
    # Read last news from the file novinky.txt
    last_news = None
    try:
        with open(news_file, 'r') as f:
            last_news = f.read().strip()
    except FileNotFoundError:
        # The script is running for the first time, the file novinky.txt doesn't exist yet.
        last_news = ''

    return last_news != news


def write_fresh_news_to_file(news: str) -> None:
    with open(news_file, 'w') as f:
        f.write(news)
        f.write('\n')


def send_message(news: str, telegram_token: str, chat_id: str) -> None:
    url = f'https://api.telegram.org/bot{telegram_token}/sendMessage'
    message = {'chat_id': chat_id, 'text': news}

    response = requests.post(url, data=message)
    if response.status_code != requests.codes.ok:
        response_json = response.json()
        print(f'Chyba: Nezdařilo se poslat Telegram zprávu: HTTP {response.status_code}', response_json['description'], file=sys.stderr)
        sys.exit(ERR_SEND_MSG)


def main():
    # Try to read Telegram config from environment variables. If environment variables are not set,
    # read Telegram config from a file.
    telegram_config = read_telegram_config_from_env()
    if not telegram_config:
        telegram_config = read_telegram_config_from_file()

    news = scrape_news_from_the_pot()
    if fresh_news(news):
        write_fresh_news_to_file(news)
        send_message(news, telegram_config['token'], telegram_config['chat_id'])
    else:
        print('Kaktus aktuálně žádnou akci nemá.')


if __name__ == '__main__':
    main()
