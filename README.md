[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=predseda_kaktus-novinky-z-kvetinace&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=predseda_kaktus-novinky-z-kvetinace)

> Projekt není aktivně vyvíjen.  
> This project is not under active development.

Skript využijete, pokud pokud máte sim kartu od [Kaktusu](https://mujkaktus.cz).
Kaktus má několikrát za rok akci (Novinky z květináče) - dobití kreditu dvojnásobkem
zaplacené částky.
Tyto akce obvykle probíhají velmi krátce. Akce jsou na [hlavní stránce](https://mujkaktus.cz/homepage)
úplně dole.

# Prerekvizity
Pro poslání zprávy na Telegram, musíte mít vytvořeného [bota](https://core.telegram.org/bots)
a [skupinu](https://core.telegram.org/api/channel) (ne kanál!), do které bude bot posílat zprávy.

## Autentizace do Telegram kanálu
Jakmile budete mít vytvořeného bota a skupinu, máte 2 možnosti.  
Buď nastavte proměnné prostředí `telegram_bot_token` a `telegram_chat_id`, nebo vytvořte konfigurační soubor `telegram.conf`,
do kterého zapište token bota a chat_id skupiny ve formátu:
```
token = <token>
chat_id = <chat_id>
```
`chat_id` skupiny můžete získat tak, že se přihlásíte do [Telegramu](https://web.telegram.org) ve webovém prohlížeči,
kliknete na příslušnou skupinu a v adresním řádku chat_id je na konci pomlčka
následovaná číslem.

Výhodou použítí proměnné prostředí místo konfiguračního souboru je, že proměnné můžete nastavit
bezpečným způsobem, například pomocí [Hashicorp Vault](https://developer.hashicorp.com/vault)
nebo [Akeyless](https://www.akeyless.io/).

# Jak skript funguje
1. Stáhne pomocí [requests](https://docs.python-requests.org/en/latest/) stránku [https://mujkaktus.cz/homepage](https://mujkaktus.cz/homepage).  
2. Vytáhne pomocí [BeautifulSoup](https://beautiful-soup-4.readthedocs.io/en/latest/)
HTML element, který obsahuje novinku.  
3. Porovná obsah souboru `novinky.txt` s poslední Novinkou z květináče na [https://mujkaktus.cz/homepage](https://mujkaktus.cz/homepage).  
**Pokud je skript spuštěn poprvé, soubor `novinky.txt` je prázdný a poslední novinku tak vyhodnotí jako novou**.  
4. Pokud se obsah souboru liší s obsahem akcí, vyhodnotí akci jako novou a pošle zprávu.  
5. Zapíše akci to souboru `novinky.txt`.

# Spuštění skriptu
1. Nainstalujte závislosti ze souboru [requirements.txt](requirements.txt) přes pip.  
Doporučuji použít [venv](https://docs.python.org/3/library/venv.html).  
2. Spusťte skript.

## Instalace závislostí pro Python (přes venv)
```
python3 -m venv venv
source venv/bin/activate
python3 -m pip install --upgrade pip
python3 -m pip install -r requirements.txt
```

## Spuštení skriptu
```
python3 kaktus.py
```

# Copyright
Zdrojový kód podléhá licenci [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode).

License: [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode)
